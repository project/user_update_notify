# User Update Notify

User Update Notify helps keep track of user profile changes and sends emails to
administrators when users in targeted roles update values in their user
profile. This is helpful for websites that closely watch user field
values for security and to ensure the information stays accurate. The module
integrates with the Token module by providing a new token to detail exactly
what has changed.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/user_update_notify).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/user_update_notify).

## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers

## Requirements

- [Token](https://www.drupal.org/project/token)

## Installation

Install User Update Notify as you would normally install a contributed
Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Navigate to Administration » Configuration » System » User Update
Notify (`/admin/config/system/user-update-notify`).
2. Toggle the "Enable email notifications" option to enable or disable email
notifications for profile changes.
3. In the "Admin Email Recipient" section, choose whether to notify a
specific user or all users of a specific role.
   - If "Specific User" is selected, enter the recipient's email in the
"Recipient Email" field.
   - If "Role" is selected, use the select list to choose which role's users
will receive notifications.
4. Use the "Email Subject" field to customize the subject line of the
notification emails.
5. Design the email body in the "Email Body" field. Utilize the
`[user:update-notify-changes]` token
   to dynamically include details of the changes made by the user.
6. In the "Targeted Roles" section, select which user roles should trigger
notifications upon making profile changes.

## Troubleshooting

If notifications are not being sent:
- Ensure the module is correctly enabled and configured.
- Check that the Drupal cron is running as expected, as it may affect email
delivery.
- Verify that the email addresses provided are correct and capable of receiving
emails.

## Maintainers

Current maintainers:
- Will Jackson - [wjackson](https://www.drupal.org/u/wjackson)

This project has been sponsored by:
- Kanopi Studios - [Kanopi.com](https://kanopi.com/)
