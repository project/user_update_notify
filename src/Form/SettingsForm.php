<?php

namespace Drupal\user_update_notify\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for setting User Update Notify module settings.
 */
class SettingsForm extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected EmailValidatorInterface $emailValidator;

  /**
   * EntityFieldManagerInterface instance.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   EntityFieldManagerInterface service.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              EmailValidatorInterface $email_validator,
                              EntityFieldManagerInterface $entityFieldManager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->emailValidator = $email_validator;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('email.validator'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_edit_notify_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('user_update_notify.settings');

    // Retrieve role options using a static call.
    $roles = $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();
    $role_options = [];
    foreach ($roles as $role_id => $role) {
      $role_options[$role_id] = $role->label();
    }

    // Define a list of field machine names to exclude.
    $exclude_fields = [
      'uid', 'uuid', 'pass', 'status', 'created', 'changed', 'access', 'login',
      'path', 'metatag', 'preferred_admin_langcode', 'preferred_langcode',
      'langcode', 'init', 'roles', 'default_langcode',
    ];

    // Retrieve field definitions and populate options for a form element.
    $user_fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $field_options = [];
    foreach ($user_fields as $field_name => $field_definition) {
      // Skip fields in the exclude list.
      if (in_array($field_name, $exclude_fields)) {
        continue;
      }

      // Ensure the field has a label before adding it.
      if (!empty($field_definition->getLabel())) {
        $field_options[$field_name] = $field_definition->getLabel();
      }
    }

    $form['notification_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Settings'),
      '#open' => TRUE,
    ];

    $form['notification_settings']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable email notifications'),
      '#description' => $this->t('Toggle on/off the capability to send email a user makes changes to their profile.'),
      '#default_value' => $config->get('enable'),
    ];

    $form['notification_settings']['admin_email_recipient'] = [
      '#type' => 'radios',
      '#title' => $this->t('Admin Email Recipient'),
      '#options' => [
        'specific_user' => $this->t('Specific User'),
        'role' => $this->t('Role'),
      ],
      '#description' => $this->t('Send notification to a specific user or all users of a role?'),
      '#default_value' => $config->get('admin_email_recipient'),
    ];

    $form['notification_settings']['recipient_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Recipient Email'),
      '#states' => [
        'visible' => [
          ':input[name="admin_email_recipient"]' => ['value' => 'specific_user'],
        ],
      ],
      '#default_value' => $config->get('recipient_email'),
    ];

    $form['notification_settings']['recipient_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Recipient Role'),
      '#options' => $role_options,
      '#states' => [
        'visible' => [
          ':input[name="admin_email_recipient"]' => ['value' => 'role'],
        ],
      ],
      '#default_value' => $config->get('recipient_role'),
    ];

    $form['notification_settings']['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#default_value' => $config->get('email_subject'),
      '#description' => $this->t('The subject of the email sent to administrators.'),
    ];

    $form['notification_settings']['email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body'),
      '#default_value' => $config->get('email_body'),
      '#description' => $this->t('The body of the email sent to administrators.'),
    ];

    $form['notification_settings']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user'],
      '#global_types' => FALSE,
      '#click_insert' => TRUE,
      '#dialog' => TRUE,
    ];

    $form['targeted_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Targeted Fields'),
      '#open' => TRUE,
    ];

    $form['targeted_fields']['targeting_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Field Targeting Choice'),
      '#options' => [
        'target_specific' => $this->t('Target only the following fields'),
        'exclude_specific' => $this->t('Target all fields except for the following'),
      ],
      '#default_value' => $config->get('targeting_type'),
      '#description' => $this->t('Choose how to specify which fields should trigger notifications.'),
    ];

    $form['targeted_fields']['selected_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('User Fields'),
      '#options' => $field_options,
      '#default_value' => $config->get('selected_fields'),
      '#description' => $this->t('Select user fields to include.'),
    ];

    $form['targeted_roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Targeted Roles'),
      '#open' => TRUE,
    ];

    $form['targeted_roles']['target_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Target Roles'),
      '#options' => $role_options,
      '#default_value' => $config->get('target_roles'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('admin_email_recipient') === 'specific_user') {
      $recipient_email = $form_state->getValue('recipient_email');
      if (!$this->emailValidator->isValid($recipient_email)) {
        $form_state->setErrorByName(
          'recipient_email',
          $this->t('The recipient email address is not valid.')
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get editable configuration.
    $config = $this->configFactory->getEditable('user_update_notify.settings');

    // Set the values from the form state.
    $config
      ->set('enable', $form_state->getValue('enable'))
      ->set('admin_email_recipient', $form_state->getValue('admin_email_recipient'))
      ->set('recipient_email', $form_state->getValue('recipient_email'))
      ->set('recipient_role', $form_state->getValue('recipient_role'))
      ->set('targeting_type', $form_state->getValue('targeting_type'))
      ->set('email_subject', $form_state->getValue('email_subject'))
      ->set('email_body', $form_state->getValue('email_body'));

    // Handle the target_roles since its checkboxes.
    $target_roles = array_filter($form_state->getValue('target_roles'));
    $target_fields = array_filter($form_state->getValue('selected_fields'));
    $config->set('target_roles', $target_roles);
    $config->set('selected_fields', $target_fields);

    // Save the configuration.
    $config->save();

    // Display a message to the user.
    $this->messenger()->addStatus($this->t('The configuration has been saved.'));
  }

}
