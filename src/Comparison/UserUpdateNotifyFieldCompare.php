<?php

namespace Drupal\user_update_notify\Comparison;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Service for comparing field values of entities to detect changes.
 */
class UserUpdateNotifyFieldCompare {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new UserUpdateNotifyFieldCompare service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityFieldManagerInterface $entityFieldManager) {
    $this->configFactory = $configFactory;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Compares the original and updated field values of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $original
   *   The original entity before the update.
   * @param \Drupal\Core\Entity\EntityInterface $updated
   *   The updated entity.
   *
   * @return array
   *   An associative array detailing changes, with field names as keys and
   *   arrays of 'original' and 'updated' values.
   */
  public function compare(EntityInterface $original, EntityInterface $updated): array {
    $changes = [];

    // Check for tracked user fields and track changes.
    foreach ($updated->getFields(FALSE) as $fieldName => $fieldItemList) {
      $tracked_fields = $this->configFactory->get('user_update_notify.settings')->get('user_fields');
      if (in_array($fieldName, $tracked_fields)) {
        $originalValues = $original->get($fieldName)->getValue();
        $updatedValues = $updated->get($fieldName)->getValue();

        if ($this->normalizeValues($originalValues) !== $this->normalizeValues($updatedValues)) {
          $changes[$fieldName] = [
            'original' => $originalValues,
            'updated' => $updatedValues,
          ];
        }
      }
    }

    return $changes;
  }

  /**
   * Normalizes field values for comparison by removing non-content properties.
   *
   * @param array $values
   *   The field values to normalize.
   *
   * @return array
   *   The normalized values.
   */
  private function normalizeValues(array $values): array {
    return array_map(function ($item) {
      unset($item['remove_button']);
      return $item;
    }, $values);
  }

  /**
   * Formats the changes for email content.
   *
   * @param array $changes
   *   The array of changes.
   *
   * @return string
   *   The formatted changes as a string.
   */
  public function formatChangesForEmail(array $changes): string {
    $formattedChanges = [];
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');

    foreach ($changes as $fieldName => $change) {
      $field_label = isset($field_definitions[$fieldName]) ?
        $field_definitions[$fieldName]->getLabel() :
        $fieldName;
      $formattedValue = "• Original: " . $this->formatValue($change['original']) .
        "\n• Updated: " . $this->formatValue($change['updated']);
      $formattedChanges[] = "$field_label ($fieldName):\n$formattedValue";
    }

    return implode("\n\n", $formattedChanges);
  }

  /**
   * Formats a single value for email content.
   *
   * @param mixed $value
   *   The value to format.
   *
   * @return string
   *   The formatted value.
   */
  protected function formatValue(mixed $value): string {
    if (is_array($value)) {
      return implode(', ', array_map([$this, 'formatValue'], $value));
    }
    return (string) $value;
  }

}
