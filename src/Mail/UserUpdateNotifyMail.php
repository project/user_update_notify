<?php

namespace Drupal\user_update_notify\Mail;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\user\UserInterface;

/**
 * Handles sending emails for the User Update Notify module.
 */
class UserUpdateNotifyMail {

  use StringTranslationTrait;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * The configuration object for 'user_update_notify.settings'.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entityType manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected entityTypeManager $entityTypeManager;

  /**
   * Constructs a new UserUpdateNotifyMail service object.
   */
  public function __construct(
    MailManagerInterface $mail_manager,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory,
    LanguageManagerInterface $languageManager,
    EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $string_translation
  ) {
    $this->mailManager = $mail_manager;
    $this->loggerFactory = $logger_factory;
    $this->config = $config_factory->get('user_update_notify.settings');
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Sends an email notification.
   *
   * @param array $params
   *   The parameters of the email.
   *
   * @return bool
   *   Return true/false if the email was successfully delivered.
   */
  public function sendEmail(array $params): bool {
    $module = 'user_update_notify';
    $key = 'user_update_notify';
    $language = $this->config->get('langcode') ?:
      $this->languageManager->getDefaultLanguage()->getId();

    // Determine the recipient(s) based on module configuration.
    if ($this->config->get('admin_email_recipient') === 'specific_user') {
      $to = $this->config->get('recipient_email');
      if (!$this->validateEmail($to)) {
        $this->loggerFactory->get('user_update_notify')
          ->error('Invalid email address: @to', ['@to' => $to]);
        return FALSE;
      }
      return $this->sendMail($module, $key, $to, $language, $params);
    }
    elseif ($this->config->get('admin_email_recipient') === 'role') {
      $notify_users = $this->getRoleNotifyUsers();
      foreach ($notify_users as $notify_user) {
        if (!$this->sendMail($module, $key, $notify_user, $language, $params)) {
          // Stops on the first failure.
          return FALSE;
        }
      }
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Validates an email address.
   *
   * @param string $email
   *   The email address to validate.
   *
   * @return bool
   *   Return true/false if the email address validates.
   */
  private function validateEmail(string $email): bool {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE;
  }

  /**
   * Retrieves the email addresses of users with the specified role.
   */
  private function getRoleNotifyUsers(): array {
    $role = $this->config->get('recipient_role');
    $user_storage = $this->entityTypeManager->getStorage('user');
    $user_ids = $user_storage->getQuery()
      ->condition('status', 1)
      ->condition('roles', $role)
      ->accessCheck(FALSE)
      ->execute();

    return array_map(function (UserInterface $user) {
      return $user->getEmail();
    }, $user_storage->loadMultiple($user_ids));
  }

  /**
   * A helper method to send an email.
   *
   * @param string $module
   *   The name of the module.
   * @param string $key
   *   The mail key to use for sending the mail.
   * @param string $to
   *   The recipient address for the email.
   * @param string $language
   *   The 2-letter ISO code for the language.
   * @param array $params
   *   The parameters for the email.
   *
   * @return bool
   *   True/false response if sending the email is successful.
   */
  private function sendMail(string $module,
  string $key,
  string $to,
                            string $language,
  array $params): bool {
    $result = $this->mailManager->mail(
      $module, $key, $to, $language, $params, NULL, TRUE
    );
    if ($result['result'] !== TRUE) {
      $this->loggerFactory->get('user_update_notify')
        ->error('Failed to send email to @to', ['@to' => $to]);
      return FALSE;
    }
    return TRUE;
  }

}
